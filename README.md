SW QUADRA PWA

Segue estrutura basica do PWA,
com exemplos de redux, redux saga, redux persist, axios, e consumindo uma API aleatoria

  * Após clone do repo
  * npm i

Para executar em ambiente de Dev

  * npm start

Para executar build de prod

  * npm run build
  * npm i -g serve
  * serve -s build