import { combineReducers } from 'redux';
import dog from './dogs/reducer';

export default combineReducers({ 
    dog
})