import { createActions, createReducer } from 'reduxsauce';
import { createSelector } from 'reselect';

export const { Types, Creators } = createActions({
    fetchDog: [],
    successFetchDog: ['dog'],
    failedFetchDog: ['error']
});

const INITIAL_STATE = {
    status: "",
    message: "",
    error: null
};

const successFetchDog = (state = INITIAL_STATE, { status, message }) => ({
    ...state, status, message
})

const failedFetchDog = (state = INITIAL_STATE, error) => ({
    ...state, error
})

export default createReducer(INITIAL_STATE, {
    [Types.SUCCESS_FETCH_DOG]: successFetchDog,
    [Types.FAILED_FETCH_DOG]: failedFetchDog,
})

export const getState = _state => _state.dog
export const getMessage = createSelector(
    getState, _state => _state.message
);