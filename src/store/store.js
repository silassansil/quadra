import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from 'redux-saga'
import rootReducer from "./reducers";
import rootSaga from "../sagas/root";

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const sagaMiddleware = createSagaMiddleware()
const persistConfig = {
    key: 'root',
    storage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

const state = createStore(
    persistedReducer,
    applyMiddleware(sagaMiddleware)
);
const persistor = persistStore(state);

sagaMiddleware.run(rootSaga);

export { state,  persistor }