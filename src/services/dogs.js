import Axios from "axios";

class Dogs {
    static async fetchDog() {
        return await Axios({
            method: 'get',
            url: `https://dog.ceo/api/breeds/image/random`,
        });
    }
}

export default Dogs;