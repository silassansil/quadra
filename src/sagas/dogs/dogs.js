import { Types } from '../../store/dogs/reducer';
import { takeLatest, put, call } from 'redux-saga/effects'
import DogsService from '../../services/dogs';

export function* fetchDog() {
    try {

        const response = yield call(DogsService.fetchDog);
        yield put({
            type: Types.SUCCESS_FETCH_DOG,
            ...response.data
        })
        return response.ok;

    } catch (error) {
        put({
            error,
            type: Types.FAILED_FETCH_DOG
        })
    }
}

export function* watchFetchDog() {
    yield takeLatest(Types.FETCH_DOG, fetchDog)
} 