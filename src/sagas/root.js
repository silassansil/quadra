import { all } from "redux-saga/effects";
import { watchFetchDog } from "./dogs/dogs";

export default function* rootSaga() {
    yield all([
        watchFetchDog()
    ])
}