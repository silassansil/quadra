import React, { Component } from 'react';
import logo from '../../logo.svg';
import '../../App.css';
import { Link } from 'react-router';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Creators, getMessage } from '../../store/dogs/reducer';



class Page extends Component {
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>{this.props.title}</h2>
                </div>
                <p className="App-intro">
                    This is the {this.props.title} page.
                </p>
                <p>
                    <Link to="/">Home</Link>
                </p>
                <p>
                    <Link to="/about">About</Link>
                </p>
                <p>
                    <Link to="/settings">Settings</Link>
                </p>

                <button onClick={() => this.props.fetchDog()}> CLIQUE </button>
                <br />
                <img src={this.props.message} alt="IMAGEM" />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    message: getMessage(state)
})
const mapDispatchToProps = dispatch =>
    bindActionCreators(Creators, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Page);